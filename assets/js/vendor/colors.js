(function ($, root, undefined) {
    
    var WINDOWS = $(window);

    var App = {
        init: function () { 
        	function buildSvg(selector, data){
	            /* Stop, When Don't Have Data */
	            if(!data || !selector){
	                return;
	            }

	            /* Start Params */
	            var d  = 22,
	                cx = d/2,
	                cy = d/2,
	                r  = d/2,
	                pieces = data.length,
	                angle = 360/data.length,
	                counter = 0;

	            /* Default Params for Ellipse */
	                xRot = 0,  // rotate by x-axis
	                lAF = 0,   // long (1)/short(0) ellipse
	                fL = 1;    // go by time line(1) / -time line(0)

	            /* Create SVG Element */
	            var svgElem = createSvg();

	            /* Create Circle */
	            svgElem.appendChild(createCircle());

	            createPath();
	            
	            /* Append SVG Element Into Selector */
	            selector.appendChild(svgElem);


	            /* Functions */
	            function createSvg(){
	                var svgElem = document.createElementNS("http://www.w3.org/2000/svg","svg");
	                        svgElem.setAttribute("viewbox", "0 0 "+d+" "+d);
	                        svgElem.setAttribute("width", d);
	                        svgElem.setAttribute("height", d);
	                        svgElem.setAttribute("class", "color-circle");
	                return svgElem;
	            };

	            function rad(deg){
	                return deg*Math.PI/180;
	            };

	            function createCircle(svgEl){
	                var circle = document.createElementNS("http://www.w3.org/2000/svg","circle");
	                        circle.setAttribute("cx", cx);
	                        circle.setAttribute("cy", cy);
	                        circle.setAttribute("r", r);
	                        circle.setAttribute("stroke", "#ccc");
	                        circle.setAttribute("stroke-width", 1);
	                    circle.setAttribute("fill", data[0].hexFormat);

	                return circle;
	            };

	            function createPath(){
	                if(counter >= pieces){
	                    return;
	                };

	                /* Create Path Group */
	                var group = document.createElementNS("http://www.w3.org/2000/svg","g");
	                svgElem.appendChild(group);

	                /* Create Path */
	                var path = document.createElementNS("http://www.w3.org/2000/svg","path");
	                group.appendChild(path);

	                /* Count End Point */
	                var toX = cx + r*Math.cos(rad(angle));
	                var toY = cy + r*Math.sin(rad(angle));

	                var d = [
	                            "M", cx+r, " ", cy,
	                            "A", r, ",", r, " ", 0, " ", (Math.abs(angle) < 180) ? 0 : 1 , " ", fL, " ", toX ,",", toY,
	                            "L", cx, ",", cy,
	                            "z"
	                        ].join("");

	                path.setAttribute("d", d);
	                path.setAttribute("fill", data[counter].hexFormat);
	                
	                group.setAttribute("transform", "rotate("+(90 + angle*counter)+", "+cx+","+cy+")");

	                /* Add Counter */
	                counter++;

	                /* Create Next Path */
	                createPath();
	            };
	        
	        }

	        var data_red_blue_gold = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#c93"
	            },
	            {
	                id: "",
	                name: "",
	                hexFormat: "#f00"
	            },
	            {
	                id: "",
	                name: "",
	                hexFormat: "#009"
	            }
	        ];

	        var data_black_white = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#000"
	            },
	            {
	                id: "",
	                name: "",
	                hexFormat: "#fff"
	            }
	        ];

	        var data_black_yellow = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#000"
	            },
	            {
	                id: "",
	                name: "",
	                hexFormat: "#ff0"
	            }
	        ];

	        var data_black_red = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#000"
	            },
	            {
	                id: "",
	                name: "",
	                hexFormat: "#f00"
	            }
	        ];

	        var data_blue = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#009"
	            }
	        ];

	        var data_grey = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#ccc"
	            }
	        ];

	        var data_yellow = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#ff0"
	            }
	        ];

	        var data_black = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#000"
	            }
	        ];

	        var data_brown = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#633"
	            }
	        ];

	        var data_green = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#096"
	            }
	        ];

	        var data_gold = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#c93"
	            }
	        ];

	        var data_pink = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#ff69b5"
	            }
	        ];

	        var data_orange = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#f60"
	            }
	        ];

	        var data_red = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#f00"
	            }
	        ];

	        var data_purple = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#939"
	            }
	        ];

	        var data_white = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#fff"
	            }
	        ];

	        var data_silver = [
	            {
	                id: "",
	                name: "",
	                hexFormat: "#ccc"
	            }
	        ];

	        Array.prototype.forEach.call(document.getElementsByClassName('red-blue-gold'), function(el) {
	            buildSvg(el, data_red_blue_gold);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('black-white'), function(el) {
	            buildSvg(el, data_black_white);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('black-red'), function(el) {
	            buildSvg(el, data_black_red);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('black-yellow'), function(el) {
	            buildSvg(el, data_black_yellow);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('blue'), function(el) {
	            buildSvg(el, data_blue);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('grey'), function(el) {
	            buildSvg(el, data_grey);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('yellow'), function(el) {
	            buildSvg(el, data_yellow);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('black'), function(el) {
	            buildSvg(el, data_black);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('brown'), function(el) {
	            buildSvg(el, data_brown);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('green'), function(el) {
	            buildSvg(el, data_green);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('gold'), function(el) {
	            buildSvg(el, data_gold);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('pink'), function(el) {
	            buildSvg(el, data_pink);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('orange'), function(el) {
	            buildSvg(el, data_orange);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('red'), function(el) {
	            buildSvg(el, data_red);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('purple'), function(el) {
	            buildSvg(el, data_purple);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('white'), function(el) {
	            buildSvg(el, data_white);
	        });
	        Array.prototype.forEach.call(document.getElementsByClassName('silver'), function(el) {
	            buildSvg(el, data_silver);
	        });
        }
    };

	WINDOWS.on('load', function () {
	    App.init();
	});

})(jQuery, this);