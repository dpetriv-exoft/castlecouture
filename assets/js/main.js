(function ($, root, undefined) {
    
    var WINDOWS = $(window),
        BODY = $('body'),
        W = WINDOWS.width(),
        H = WINDOWS.height(),
        SCROLL = WINDOWS.scrollTop();    

    var App = {
        init: function () { 
            if (W < 993) {
                this.li_mobile_click();
                this.mobile_checkout_click();
                this.mobile_category_click();
            } else {
                this.li_click(); 
                this.checkout_click();   
            }
            $('.sub-show + ul').parent('li').addClass('no-padding');
            this.form_search(); 
            this.sticky_header();  
            this.mobile_menu();
            this.var_quantity(); 
            this.filter_click();
            this.color_click();
            this.sizes_click();
            this.btn_close();
            this.sidebar_click();
            this.search_autocomplete();
            this.mob_aside();
            this.event_sorting();
            this.popover();

            $('.slider-top').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: true,
                arrows: true,
                nextArrow: '<span class="slick-next slick-arrow"></span>',
                prevArrow: '<span class="slick-prev slick-arrow"></span>', 
                responsive: [
                    {
                      breakpoint: 993,
                      settings: {
                        arrows: false,
                        dots: true
                      }
                    }
                ]
            });

            $('.slider-products').slick({
                centerMode: true,
                centerPadding: '85px',/*6.77vw*/
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: true,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>',
                responsive: [
                    {
                      breakpoint: 993,
                      settings: {
                        slidesToShow: 2
                      }
                    }
                ]
            });

            $('.bottom-slider').slick({
                centerMode: true,
                centerPadding: '125px',/*10.1vw*/
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: true,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>'
            });

            $('.bottom-slider-mobile').slick({
                centerPadding: '130px',
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: true,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>'
            });

            $('.product-slider_content').slick({
                centerMode: true,
                centerPadding: '0',
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: true,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>',
                responsive: [
                    {
                      breakpoint: 993,
                      settings: {
                        slidesToShow: 2
                      }
                    }
                ]
            });            
            
            $('.slider-nav').slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                centerPadding: '0',
                dots: false,
                centerMode: false,
                focusOnSelect: false,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>'
            });

            $('.viewer-content').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                centerPadding: '0',
                dots: false,
                centerMode: false,
                focusOnSelect: false,
                arrows: true,
                nextArrow: '<div class="slick-next slick-arrow"><span></span></div>',
                prevArrow: '<div class="slick-prev slick-arrow"><span></span></div>',
                responsive: [
                    {
                      breakpoint: 993,
                      settings: {
                        arrows: false
                      }
                    }
                ]
            });

            setTimeout(function() {
                if($('select').length == 0){
                    return;
                };
                $('select').styler();
            }, 50);

            this.my_current();

            if($("#promoCarousel") && $("#promoCarousel")[0]){
                $("#promoCarousel").swipe({
                    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
                        if (direction == 'left') $(this).carousel('next');
                        if (direction == 'right') $(this).carousel('prev');
                    },
                    allowPageScroll:"vertical"
                });
            };
        },
        color_click: function(some) {
            var color = $('.color-spin'),
                sp = $('.colors h6 span');

            color.click(function(){
                if(!$(this).hasClass('disable')) {
                    color.removeClass('active');
                    $(this).addClass('active');
                    sp.html($(this).attr('data-fill'));
                }
            });
        },
        my_current: function() {
            var pic = $('.slider-nav .slick-slide > div'),
                big = $('.slider-for');
            pic.click(function(){
                pic.removeClass('my-current');
                var th = $(this);
                th.addClass('my-current');
                big.css('background-image', th.css('background-image'));    
            });
        },
        popover: function() {
            $('a[rel=popover]').click(function(e){
                $('a[rel=popover]').not($(this)).popover('hide');
                $(this).popover('toggle');
            }).popover({
                html: 'true',
                trigger: 'manual'
            }).on('shown.bs.popover', function(e){
                var popover = $(this);
                $(this).parent().find('.close-popover').on('click', function(e){
                    popover.popover('hide');
                });
            });
        },
        sizes_click: function() {
            var sp = $('.sizes-wrapper p span'),
                li = $('.sizes li');

            li.click(function(){
                if(!$(this).hasClass('disable')) {
                    li.removeClass('active');
                    $(this).addClass('active');
                    sp.html($(this).html());
                }
            });
        },
        btn_close: function() {
            $('.btn-close').click(function(){    
                $(this).parent().fadeOut().parent().removeClass('filter-open');
            });
        },
        mob_aside: function() {
            var cat = $('.categories'),
                cat_click = $('.mob-aside p:first-child'),
                cat_close = cat.find('.close');
                latest = $('.latest-posts'),
                latest_click = $('.mob-aside p:last-child');
                latest_close = latest.find('.close');

            cat_click.click(function(){
                cat.fadeIn();
                BODY.addClass('hidden');
            });
            cat_close.click(function(){
                cat.fadeOut();
                BODY.removeClass('hidden');
            });
            latest_click.click(function(){
                latest.fadeIn();
                BODY.addClass('hidden');
            });
            latest_close.click(function(){
                latest.fadeOut();
                BODY.removeClass('hidden');
            });
        },
        li_click: function() {
            $('.show-list > li > span').click(function(){
                $('.show-list > li > div').fadeOut().parent().removeClass('filter-open');
                $(this).parent().addClass('filter-open').find('span + div').fadeIn();
            });
        },
        li_mobile_click: function() {
            var count = 0;
            $('.show-list > li > span').click(function(){
                var par = $(this).parent();
                count++;    
                if (count % 2 == 0) {
                    par.removeClass('li-active').find('span + div').slideUp();
                } else {
                    $('.show-list > li').removeClass('li-active').find('span + div').slideUp();
                    par.addClass('li-active').find('span + div').slideDown();
                }
            });
        },
        filter_click: function() {
            $('#filter-mobile_filter').click(function(){
                $('#main-filters').fadeIn();
                $('body').css('overflow', 'hidden');
            });
            $('#filter-mobile_sort').click(function(){
                $('#sort-by').fadeIn();
                $('body').css('overflow', 'hidden');
            });
            $('#filter-mobile_category').click(function(){
                $('.listing-sidebar').fadeIn();
                $('body').css('overflow', 'hidden');
            });
            $('.filters h6 .close').click(function(){
                $('#main-filters').fadeOut();
                $('#sort-by').fadeOut();
                $('.listing-sidebar').fadeOut();
                $('body').css('overflow', 'visible');
            });
            $('.listing-sidebar h6 .close').click(function(){
                $('#main-filters').fadeOut();
                $('#sort-by').fadeOut();
                $('.listing-sidebar').fadeOut();
                $('body').css('overflow', 'visible');
            });
        },
        checkout_click: function() {
            var checkout = $('.checkout'),
                checkout_pop = $('.checkout_pop-up'),
                close = $('.checkout_pop-up .close');

            checkout.click(function(){
                checkout_pop.fadeIn();
            });

            close.click(function(){
                checkout_pop.fadeOut();
            });
        },
        sidebar_click: function() {
            var h = $('.to-show h5');

            h.click(function(){
                $(this).parent().find('>ul').slideToggle();
            });
        },
        form_search: function () {      
            $('.search-form > img').click(function() {
                $(this).parent().addClass('active');
                $('.ui-wrapper').css('display', 'block');
            });
            $('.close-form_search').click(function(){
                $(this).parent().parent().removeClass('active');
                $('.ui-wrapper').css('display', 'none');
            });
        },
        search_autocomplete: function() {
            var languages = ["Sherri Hill 500123","Sherri Hill 500124", "Sherri Hill 500125", "Sherri Hill 500126","Sherri Hill 500127",
                             "Sherri Hill 500128","Sherri Hill 500129","Sherri Hill 500130","Sherri Hill 500131","Sherri Hill 500132"]; 

            $("#form-input").autocomplete({
                source: languages
            });
        },
        sticky_header: function () {
            $(window).scroll(function(){
              var sticky = $('header'),
                  top_img = $('.top-logo img'),
                  scroll = $(window).scrollTop();

              if (scroll >= 10) {
                sticky.addClass('fixed-header');
                top_img.attr('src', 'assets/img/home/menu_logo_brown.png');
              } else {
                sticky.removeClass('fixed-header');
                top_img.attr('src', 'assets/img/home/menu_logo.png');
              }
            });
        },
        mobile_menu: function() {
            var open = $('.open-menu'),
                close = $('.close-menu'),
                menu = $('menu');

            open.click(function(){
                menu.addClass('menu-active');
                BODY.addClass('hidden');
            });
            close.click(function(){
                menu.removeClass('menu-active');
                BODY.removeClass('hidden');
            });
        },
        mobile_checkout_click: function() {
            var checkout = $('.checkout'),
                checkout_pop = $('.checkout_pop-up'),
                close = $('.checkout_pop-up .close');

            checkout.click(function(){
                checkout_pop.fadeIn();
                BODY.addClass('hidden');
            });
            close.click(function(){
                checkout_pop.fadeOut();
                BODY.removeClass('hidden');
            });
        },
        event_sorting: function() {

          var $container = $('.all-events');

          $container.isotope({
            itemSelector: ".event-post"
          })

          var $optionSets = $('.event-filter'),
            $optionLinks = $optionSets.find('a');

          $optionLinks.click(function() {
            var $this = $(this);
            // don't proceed if already selected
            if ($this.hasClass('selected')) {
              return false;
            }
            var $optionSet = $optionSets/*$this.parents('.option-set')*/;
            $optionSet.find('.selected').removeClass('selected');
            $this.addClass('selected');

            // make option object dynamically, i.e. { filter: '.my-filter-class' }
            var options = {},
              key = $optionSet.attr('data-option-key'),
              value = $this.attr('data-option-value');
            // parse 'false' as false boolean
            value = value === 'false' ? false : value;
            options[key] = value;
            if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
              // changes in layout modes need extra logic
              changeLayoutMode($this, options)
            } else {
              // otherwise, apply new options
              $container.isotope(options);
            }

            return false;
          });
        },
        var_quantity: function() {
            $(".button").on("click", function() {

              var $button = $(this);
              var oldValue = $button.parent().find("input").val();

              if ($button.find('i').hasClass('fa-plus')) {
                  var newVal = parseFloat(oldValue) + 1;
                } else {
               // Don't allow decrementing below zero
                if (oldValue > 0) {
                  var newVal = parseFloat(oldValue) - 1;
                } else {
                  newVal = 0;
                }
              }

              $button.parent().find("input").val(newVal);

            });
        },
        mobile_category_click: function() {
            $('.to-show > h5').click(function(){
                $(this).parent().toggleClass('li-active');
            });
            $('.sub-show').click(function() {
                $(this).parent().toggleClass('show-active');
            });
        }
    };
    
    WINDOWS.on('load', function () {
        App.init();
        function separateColorBlocks(containers){
            containers.each(function(i, v){
                var c = colorScroll(v);
            });
        };
        function colorScroll(container){
            var left = 0;
            var box = $(container)
            var el = box.find('.color');

            // console.log(box, el);

            var leftBtn = box.children('.left');
            var rightBtn = box.children('.right');

            var goLeft = function(){
                if(el.width() - box.width() > -left){
                    left-=35;
                    el.css('left', left + 'px');
                };
            };

            var goRight = function(){
                if(left < 0){
                    left+=35;
                    el.css('left', left + 'px');
                };
            };

            // console.log(box.width(), el.width());

            if(box.width() < el.width()){
                // left=30;
                el.css('left', left + 'px');

                leftBtn.show();
                rightBtn.show();

                leftBtn.click(goRight);
                rightBtn.click(goLeft);
            }
        };
        setTimeout(function(){
            var color = separateColorBlocks($('.color-container'));
        }, 100);

        $('#promoCarousel').on('slid.bs.carousel', function () {
            MagicZoom.refresh();
        });
        
        function carouselScroll(container, el){
            var top = 0;
            var count = el.children().length;
            if(count > 5){
                $('button.bottom').show();
            };

            var goTop = function(){
                if(count > 5 && (top < 0)){
                    top+=110;
                    el.css('top', top + 'px');
                    $('button.bottom').show();

                    if(top == 0){
                        $('button.top').hide();
                    };
                };
            };

            var goBottom = function(){
                if(count > 5 && (el.height() - container.height() > -top+10)){
                    top-=110;
                    el.css('top', top + 'px');
                    $('button.top').show();

                    if(el.height() - container.height() <= -top+10){
                        $('button.bottom').hide();
                    }
                }
            };

            return {
                top: goTop,
                bottom: goBottom
            };
        };

        var x = carouselScroll($('.scrollable'), $('.carousel-indicators'));
        $('.scrollable .top').click(x.top);
        $('.scrollable .bottom').click(x.bottom);
    });

    WINDOWS.on('load', function () {
        BODY.removeClass('loading');
    });

})(jQuery, this);


