// Include gulp
var gulp            = require('gulp');
var sass            = require('gulp-sass');
var assets          = require('postcss-assets');
var postcss         = require('gulp-postcss');
var watch           = require('gulp-watch');
var autoprefixer    = require('gulp-autoprefixer');
var sourcemaps      = require('gulp-sourcemaps');
var notify          = require("gulp-notify");
var browserSync     = require('browser-sync');
var reload          = browserSync.reload;
var pngquant        = require('imagemin-pngquant');
var cache           = require('gulp-cache');
var imagemin        = require('gulp-imagemin');

// Compile Our Sass 
gulp.task('sass', function() {
  
  return gulp.src('assets/sass/**/*.scss')  
      .pipe(sourcemaps.init())    
      .pipe(sass({ errLogToConsole: true}))
      .on('error', notify.onError('<%= error.message %>'))
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: true
      })) 
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest('assets/css'))
     .pipe(reload({stream:true}));
    
});
 
gulp.task('imgmin', function() {
    return gulp.src('assets/img/*')
        .pipe(cache(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('assets/img/'));
});

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
        './style.css',
        './*.html'
    ];

    //initialize browsersync
    browserSync.init(files, {
        //browsersync with a php server
        proxy: "localhost/castlecouture/my-account-info.html",
        notify: false
    });
});
 
gulp.task('watch', function() {
  gulp.watch('assets/sass/**/*.scss', ['sass']);
});

// Default Task 
gulp.task('default', ['sass', 'browser-sync', 'watch']); 
